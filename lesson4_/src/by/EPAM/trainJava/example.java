package by.EPAM.trainJava;

import java.util.Scanner;
/***
 * ������ 4. ������� 1.
 * ������������� Student / Customer
 */

public class example {// ������, Code COnvention - ��� ���-�� �� ���������. ����� � ��������� �����
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Student s0= new Student();
		Student s1= new Student(2,"alex","sergeevich","pskn",1799,6,6,"king village","none","literature", "poems");
		
		Customer c0= new Customer();
		Customer c1= new Customer(3,"klop","sergeevich","klopovsky","6 ave", "12135-541-14246", "999-9-999");
		
		int v=-1;
		System.out.println("����� ���������� � ��������� ����� �������?");
		if(scanner.hasNextInt()) {
			v= scanner.nextInt();
		}else{
			scanner.next();
		}
		
		s0.show(v);
		s1.show(v);
		
		System.out.println("����� ���������� � �������� ����� �������?");
		if(scanner.hasNextInt()) {
			v= scanner.nextInt();
		}else{
			scanner.next();
		}
		c0.show(v);
		c1.show(v);
		
	}
}
