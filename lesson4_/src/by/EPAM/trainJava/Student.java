package by.EPAM.trainJava;

import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/***
 * 
 * @author Sergii_Kotov
 * ëåêöèÿ 4. çàäàíèå 1
 */
public class Student {
	private int id;
	private String surname, name, secName;// каждую переменную объявляем в отдельной строке
	private Date dBirth;
	private String addr, phoneNumber, facultet, course;

	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		
	public Student () {
		this(0,"","","",2000,1,1,"","","","");
	}
	
	public Student (int i, String sr, String sn, String snn, int year, int mn, int d,
			String adr, String phn, String fc, String cr) {
		id =i;
		surname =sr; name=sn; secName=snn;
		Calendar c=Calendar.getInstance();
		c.set(year,mn,d);
		dBirth=c.getTime();
		addr=adr;  phoneNumber=phn;  facultet=fc;  course=cr; 
	}
	
	
	public void show (int whatToShow){
		switch (whatToShow) {
		case 0: 
			System.out.println("ÔÈÎ: "+surname + " " + name + " " + secName);
			break;
		case 1:
			System.out.println("êóðñ/ôàêóëüòåò: "+course + " " + facultet);
			break;
		case 2:
			System.out.println("ëè÷íûå äàííûå: "+surname + " " + name + " " + secName);
			System.out.println("äàòà ðîæäåíèÿ: "+ sdf.format(dBirth) + " òåë. íîìåð " + phoneNumber);
			break;
		default: System.out.println("ÔÈÎ: "+surname + " " + name + " " + secName);
				 System.out.println("êóðñ/ôàêóëüòåò: "+course + " " + facultet);
		}
		 System.out.println("ñðåäíÿÿ îöåíêà: "+getCountAvgMark());
	}
	
	public double getCountAvgMark () {
		return Math.random()*10;
	}
	
	public void setId (int i) {
		id=i;
	}

	public void setSurname (String s) {
		surname=s;
	}

	public void setName (String s) {
		name=s;
	}
	
	public void setSecName (String s) {
		secName=s;
	}
	
	public void setPhoneNumber (String s) {
		phoneNumber=s;
	}
	
	public void setFacultet (String s) {
		facultet=s;
	}
		
	public void setCourse (String s) {
		course=s;
	}
	
	public int getId() {
		return id;
	}
		
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSecName() {
		return secName;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public String getFacultet() {
		return facultet;
	}
	
	public String getCourse() {
		return course;
	}
	
	public String getAddr() {
		return addr;
	}
	
	public Date getdBirth() {
		return dBirth;
	}
}
