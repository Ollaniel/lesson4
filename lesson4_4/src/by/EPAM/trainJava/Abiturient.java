package by.EPAM.trainJava;

import java.util.concurrent.ThreadLocalRandom;

/***
 * 
 * @author Sergii_Kotov
 * ������ 4. ������� 4
 * �������� �����������. 
 * ����������� ����������� �� �������� ����.
 */
public class Abiturient implements Comparable {
	/***
	 * ���������� ���������
	 */
	public static int numCourses =4;
	/***
	 * ��� ����������� 
	 */
	private int id; 
	/***
	 * ���
	 */
	private String surname, name, secName;
	/***
	 * ������ ������
	 */
	private int [] marks = new int [numCourses];
	/***
	 * �������� �����������
	 * @param i ��� �����������
	 * @param sr �������
	 * @param sn ���
	 * @param snn ��������
	 */
	public Abiturient (int i, String sr, String sn, String snn) {
		id =i;
		surname =sr; name=sn; secName=snn;
	}

	/***
	 * ��� �������� ��������� ������������
	 * @param i	��� ��������
	 * @param igr ����� 
	 */
	public Abiturient (int i) {
		id =i;
		surname ="Surname_"+i; name="Name_"+i; secName="Secname_"+i;
		for (int j = 0; j < marks.length; j++) {
			marks[j]=ThreadLocalRandom.current().nextInt(2, 7); // ����� ���� �������� �������
			if (marks[j]>5) {
				marks[j]=5;
			}
		}
	}
	
	/***
	 * �� ������� ������ marks ������� ������� ������ 
	 * @return
	 */
	public double getAvgMark (){
		double t=0;
		for (int j = 0; j < marks.length; j++) {
			t+=marks[j];
		}
		return Math.round(t/marks.length*10)/10;
	}
	
	/***
	 * ��� ���������� ������� �� ��������
	 * @param obj
	 * @return 0 ����� 1 ������ -1 ������
	 */
	public int compareTo(Object obj)
	  {
		Abiturient tmp = (Abiturient)obj;
		return -1*Double.compare(this.getAvgMark(),tmp.getAvgMark());
	  }
		
	/***
	 * 
	 * @return 	������ � ��� � ����������� ���������
	 */
	public String printAllInfo() {
		return "id="+id+ "\t���="+surname+ " "+name+" "+secName+ ".\t\t������� ���� = " + getAvgMark();
	}
	
	public void setId(int i) {
		id = i;
	}
	
	public void setSurname (String s) {
		surname=s;
	}

	public void setName (String s) {
		name=s;
	}
	
	public void setSecName (String s) {
		secName=s;
	}

	public int getId() {
		return id;
	}
		
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSecName() {
		return secName;
	}
}
