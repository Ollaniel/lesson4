package by.EPAM.trainJava;
/***
 * 
 * @author Sergii_Kotov
 * ������ 4. ������� 3
 * �������� ������ � ������� ������ ��������. 
 * �������� ������ �� ���������, ����� ������������ ���-�� ����. 
 */
public class Group {
	/***
	 * ��� ������
	 */
	private int groupId;
	private String groupName;
	/***
	 * ������������ ���������� ��������� � ������
	 */
	private int capacity;
	/***
	 * ������ ������ �� ���������
	 */
	private Student[] students;
	
	/***
	 * �������� �������� � ������
	 * @param st ������ �� ��������
	 * @return ���� ����� � ������ ����, �� true (��� ��������) ����� �� �������� � ��������� - false
	 */
	public boolean addStudent (Student st){
		for (int i = 0; i < students.length; i++) {
			if (students[i]==null) {
				// ����� ��� �������� �������
				students[i]=st;
				return true;
			}
		}
		// ��� ����� � ������
		return false;
	}
	/***
	 * ����������� �� ���������
	 */
	public Group (){
		this(1,"������",8);
	}
	
	/***
	 * �������� �����������.
	 * @param id
	 * @param nm
	 * @param cp
	 */
	public Group (int id, String nm, int cp){
		groupId=id;
		groupName =nm;
		capacity=cp;
		students = new Student[capacity];
	}
	
	/***
	 * ����� �������� ���������� � ������.
	 * ������� �������� ����� ��� ������.
	 */
	public void printGroup () {
		double avgGroup=0;
		int actualL=0;

		System.out.println("id:"+ groupId+" ��������:"+groupName+ " ���������� ����:"+ capacity+ " ������������� ������ (�����: ���_��������)");		
		for (int i = 0; i < students.length; i++) {
			if (students[i]!=null) {
				avgGroup+=students[i].getAvgMark();
				actualL++;
				System.out.print("("+(i+1)+": "+students[i].getId()+") ");
			}
		}
		avgGroup=avgGroup/actualL;
		System.out.println();
		System.out.println("������� ���� ������ = "+ Math.round(avgGroup*10)/10 );
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public int getGroupId() {
		return groupId;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	/***
	 * ������� �������� �� ������. 
	 * @param i ��� ��������
	 * @return ������ �� �������� 
	 */
	public Student getStudent(int i) {
			return students[i];
	}

}
